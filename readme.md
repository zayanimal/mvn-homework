# Задание на практическую работу по модулю "Сборка приложений"

### Задание #1 (Maven)
 - Вынести определение версий зависимостей в блок properties;
 - Настроить зависимости Spring так, чтобы не было дубликатов и в сборке была зависимость org.springframework:spring-core:5.3.16 (удалять зависимости из pom.xml нельзя, итоговая конфигурация РОМ должна быть оптимальной)
 - Сделать зависимость junit доступной только в каталоге src/test/java;
 - Добавить в проект settings.xml;
 - В settings.xml определить единый репозиторий для всех зависимостей (репозиторий можно взять любой).

### Задание #2 (Maven)
 - Добавить контроллер с именем MyDebugController;
 - Создать профиль debug;
 - При сборке в профиле debug в проекте должен остаться контроллер MyDebugController (контроллер MyTargetController должен быть исключен из сборки);
 - В сборке с профилем debug не должно быть пакета sbp.school.jmx (включая его содержимое);
 - При запуске приложения, собранного с профилем debug, в контексте Spring не должно быть зарегистрировано бинов appStatisticsBean и jmxExporter;

### Условия
 - Результат работы программы после доработок/рефакторинга (без профиля debug) должен остаться прежним.

### Как работать над заданием?
 - Создаёте от master свою новую ветку с типом release и с именем по шаблону "Фамилия_Модуль_Задание" (например: release/taranov_maven_1);
 - Клонируете проект из новой ветки в локальный репозиторий;
 - Делаете доработки у себя в локальном репозитории;
 - Заливаете свои доработки в новую feature ветку (например feature/taranov_maven_1);
 - Делаете PR в свою исходную релизную ветку (из которой клонировали проект);

### Условие успешной сдачи работы (критерии приемки)
 - Проект компилируется и запускается;
 - Результат review PR = approve.
 